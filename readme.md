# Benchmark
Jest to prosty benchmark stworzony na potrzeby pracy magisterskiej.
## Przygotowanie i kompilacja
Pierwsze co będzie potrzebne do kompilacji to język programowania Rust. Najprościej jest go zainstalować, używając narzędzia rustup. 

```bash
curl https://sh.rustup.rs -sSf | sh
```

Możliwe, że będzie potrzebne dodanie cargo do zmiennej środowiskowej PATH, więcej można dowiedzieć się [tu](https://www.rust-lang.org/pl-PL/install.html).

Jeśli nie mamy kompozytora na swojej dystrybucji trzeba go także doinstalować, najprościej będzie zainstalować westona:
- apt install weston
- dnf install weston

Program był uruchamiany na fedorze, która już ma Waylanda, jeśli program będzie kompilowany na czymś innym na pewno będą potrzebne biblioteki, które mogą być już dołączone przy instalacji westona:
```bash
libwayland-client
```

Następnie będzie potrzeba doinstalować biblioteki developerskie:
```bash
pam-devel, mtdev-devel,mesa-libgbm-devel,mesa-libGLES-devel ,gtk3-devel,gdk-pixbuf2-xlib ,pango-devel,atk-devel,gdk-pixbuf2-devel,cairo-devel,dbus-devel ,mesa-libEGL-devel,mesa-libwayland-client-devel
```
Potem trzeba wejść na katalog z projektem i go skompilować poleceniem
```bash
cargo build
```

Jeśli będzie brakowało nam jeszcze jakichś bibliotek kompilator powinien dać znać o tym.

## Uruchamianie

Benchmark powinno się uruchamiać z wiersza poleceń używając komendy

```bash
cargo run
```

Jeśli chcemy zmienić rozmiary okna możemy użyć dodatkowo argumentów

```bash
cargo run -- --size=320x240
```
lub gdy chcemy uruchomić program w trybie pełno ekranowym

```bash
cargo run -- --fullscreen
```
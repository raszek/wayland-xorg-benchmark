use glium;
use glium::Surface;

use xml::reader::{EventReader, XmlEvent};

use vertex::{TexturedVertex, Vertex};

use std::fs::File;
use std::io::BufReader;

pub fn view_matrix(position: &[f32; 3], direction: &[f32; 3], up: &[f32; 3]) -> [[f32; 4]; 4] {
    let f = {
        let f = direction;
        let len = f[0] * f[0] + f[1] * f[1] + f[2] * f[2];
        let len = len.sqrt();
        [f[0] / len, f[1] / len, f[2] / len]
    };

    let s = [up[1] * f[2] - up[2] * f[1],
             up[2] * f[0] - up[0] * f[2],
             up[0] * f[1] - up[1] * f[0]];

    let s_norm = {
        let len = s[0] * s[0] + s[1] * s[1] + s[2] * s[2];
        let len = len.sqrt();
        [s[0] / len, s[1] / len, s[2] / len]
    };

    let u = [f[1] * s_norm[2] - f[2] * s_norm[1],
             f[2] * s_norm[0] - f[0] * s_norm[2],
             f[0] * s_norm[1] - f[1] * s_norm[0]];

    let p = [-position[0] * s_norm[0] - position[1] * s_norm[1] - position[2] * s_norm[2],
             -position[0] * u[0] - position[1] * u[1] - position[2] * u[2],
             -position[0] * f[0] - position[1] * f[1] - position[2] * f[2]];

    [
        [s[0], u[0], f[0], 0.0],
        [s[1], u[1], f[1], 0.0],
        [s[2], u[2], f[2], 0.0],
        [p[0], p[1], p[2], 1.0],
    ]
}

pub fn calc_perspective(target: &glium::Frame) -> [[f32; 4]; 4] {

    let (width, height) = target.get_dimensions();
    let aspect_ratio = height as f32 / width as f32;

    let fov: f32 = 3.141592 / 3.0;
    let zfar = 1024.0;
    let znear = 0.1;

    let f = 1.0 / (fov / 2.0).tan();

    [
        [f *   aspect_ratio   ,    0.0,              0.0              ,   0.0],
        [         0.0         ,     f ,              0.0              ,   0.0],
        [         0.0         ,    0.0,  (zfar+znear)/(zfar-znear)    ,   1.0],
        [         0.0         ,    0.0, -(2.0*zfar*znear)/(zfar-znear),   0.0],
    ]
}

fn parse_collada(path: &str) -> (Vec<String>, String) {
    
    let file = File::open(path).expect("Could not find the file");
    let file = BufReader::new(file);

    let parser = EventReader::new(file);

    let mut tag_content = String::new();

    let mut floats: Vec<String> = Vec::new();
    let mut indices_str = String::new();

    for e in parser {
        match e {
            Ok(XmlEvent::Characters(content)) => {
                tag_content = content;
            }
            Ok(XmlEvent::EndElement { name }) => {
                match name.local_name.as_str() {
                    "float_array" => floats.push(tag_content.clone()), 
                    "p" => indices_str = tag_content.clone(),
                    _ => (),
                }
            }
            Err(e) => {
                println!("Error: {}", e);
                break;
            }
            _ => {}
        }
    }

    (floats, indices_str)
}

pub fn create_textured_shape(path: &str) -> Vec<TexturedVertex> {

    let (floats, indices_str) = parse_collada(path);

    let positions_floats = floats[0].split_whitespace().map(|s| s.parse::<f32>().unwrap() ).collect::<Vec<_>>();
    let normals_floats = floats[1].split_whitespace().map(|s| s.parse::<f32>().unwrap() ).collect::<Vec<_>>();
    let tex_coords_floats = floats[2].split_whitespace().map(|s| s.parse::<f32>().unwrap() ).collect::<Vec<_>>();
    let indices = indices_str.split_whitespace().map(|s| s.parse::<u16>().unwrap() ).collect::<Vec<_>>();

    let mut positions: Vec<(f32, f32, f32)> =  Vec::with_capacity(positions_floats.len()/3);
    let mut normals: Vec<(f32, f32, f32)> = Vec::with_capacity(normals_floats.len()/3);
    let mut tex_coords: Vec<(f32, f32)> = Vec::with_capacity(tex_coords_floats.len()/2);

    let mut vertices: Vec<TexturedVertex> = Vec::with_capacity(indices.len()/3);

    for i in (0..positions_floats.len()/3).map(|i| i * 3) {
        positions.push((positions_floats[i], positions_floats[i+1], positions_floats[i+2]));
    }

    for i in (0..normals_floats.len()/3).map(|i| i * 3) {
        normals.push((normals_floats[i], normals_floats[i+1], normals_floats[i+2]));
    }

    for i in (0..tex_coords_floats.len()/2).map(|i| i * 2) {
        tex_coords.push((tex_coords_floats[i], tex_coords_floats[i+1]));
    }

    for i in (0..indices.len()/3).map(|i| i * 3) {
        vertices.push(
            TexturedVertex{
                position: positions[indices[i] as usize],
                normal: normals[indices[i+1] as usize],
                tex_coords: tex_coords[indices[i+2] as usize],
            }
        );
    }

    vertices
}


pub fn create_shape(path: &str) -> Vec<Vertex> {

    let (floats, indices_str) = parse_collada(path);

    let positions_floats = floats[0].split_whitespace().map(|s| s.parse::<f32>().unwrap() ).collect::<Vec<_>>();
    let normals_floats = floats[1].split_whitespace().map(|s| s.parse::<f32>().unwrap() ).collect::<Vec<_>>();
    let indices = indices_str.split_whitespace().map(|s| s.parse::<u16>().unwrap() ).collect::<Vec<_>>();

    let mut positions: Vec<(f32, f32, f32)> =  Vec::with_capacity(positions_floats.len()/3);
    let mut normals: Vec<(f32, f32, f32)> = Vec::with_capacity(normals_floats.len()/3);

    let mut vertices: Vec<Vertex> = Vec::with_capacity(indices.len()/2);

    for i in (0..positions_floats.len()/3).map(|i| i * 3) {
        positions.push((positions_floats[i], positions_floats[i+1], positions_floats[i+2]));
    }

    for i in (0..normals_floats.len()/3).map(|i| i * 3) {
        normals.push((normals_floats[i], normals_floats[i+1], normals_floats[i+2]));
    }

    for i in (0..indices.len()/2).map(|i| i * 2) {
        vertices.push(
            Vertex{
                position: positions[indices[i] as usize],
                normal: normals[indices[i+1] as usize],
            }
        );
    }

    vertices
}

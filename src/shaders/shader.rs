pub struct Shader {
    pub vertex_shader_src: &'static str,
    pub fragment_shader_src: &'static str,
}

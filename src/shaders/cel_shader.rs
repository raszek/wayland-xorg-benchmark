use shaders::shader::Shader;

pub fn get_shader() -> Shader {

let vertex_shader_src = r#"
        #version 150

        in vec3 position;
        in vec3 normal;

        out vec3 v_normal;
        out vec3 v_position;

        uniform mat4 perspective;
        uniform mat4 view;
        uniform mat4 model;

        void main() {
            mat4 modelview = view * model;
            v_normal = transpose(inverse(mat3(modelview))) * normal;
            gl_Position = perspective * modelview * vec4(position, 1.0);
            v_position = gl_Position.xyz / gl_Position.w;
        }
    "#;

    let fragment_shader_src = r#"
        #version 150

        in vec3 v_normal;
        in vec3 v_position;

        out vec4 color;

        uniform vec3 u_light;

        const vec3 ambient_color = vec3(0.2, 0.0, 0.0);
        const vec3 diffuse_color = vec3(0.6, 0.0, 0.0);
        const vec3 specular_color = vec3(1.0, 1.0, 1.0);
        const float shininess = 16.0;

        const float A = 0.1;
	    const float B = 0.3;
	    const float C = 0.6;
	    const float D = 1.0;


        float step(float edge, float x)
        {
            return x < edge ? 0.0 : 1.0;
        }

        void main() {
            vec3 N = normalize(v_normal);           
            vec3 L = normalize(u_light);           
            vec3 H = normalize(normalize(u_light) + normalize(-v_position));

            float df = max(0.0, dot(N, L));

            if (df < A) df = 0.0;
            else if (df < B) df = B;
            else if (df < C) df = C;
            else df = D;
            float sf = max(0.0, dot(N, H));
            sf = pow(sf, shininess);
            sf = step(1.0, sf);

            color = vec4(ambient_color + df * diffuse_color + sf * specular_color, 1.0);
        }
    "#;
    
    Shader { vertex_shader_src: vertex_shader_src, fragment_shader_src: fragment_shader_src }
}

use vertex;
use shaders;

use mvp::{view_matrix, calc_perspective, create_shape};

use std::time::{Instant};

use glium;
use glium::Surface;
use glium::glutin::{Event};

use shading::Shading;


pub struct Test {
    shape: glium::VertexBuffer<vertex::Vertex>,
    program: glium::Program,
}

impl Test {

    pub fn with_shading(display: &glium::backend::glutin_backend::GlutinFacade, shape_path: &str, shading: Shading) -> Test {

        let shape = create_shape(shape_path);
        let shape = glium::VertexBuffer::new(display, &shape).unwrap();

        let program = match shading {
            Shading::Gouraud => {
                let shader = shaders::gouraud_shader::get_shader();
                glium::Program::from_source(display, shader.vertex_shader_src, shader.fragment_shader_src, None).unwrap()
            },
            Shading::BlingPhong => {
                let shader = shaders::bling_phong_shader::get_shader();
                glium::Program::from_source(display, shader.vertex_shader_src, shader.fragment_shader_src, None).unwrap()
            },
            Shading::Cel => {
                let shader = shaders::cel_shader::get_shader();
                glium::Program::from_source(display, shader.vertex_shader_src, shader.fragment_shader_src, None).unwrap()
            },
        };

        Test {shape: shape, program: program}
    }

    pub fn run(self, display: &glium::backend::glutin_backend::GlutinFacade,
                            f: &Fn(f32) -> ([[f32; 4]; 4], [[f32; 3]; 3], [f32; 3])) {

        let params = glium::DrawParameters {
            depth: glium::Depth {
                test: glium::draw_parameters::DepthTest::IfLess,
                write: true,
                .. Default::default()
            },
            .. Default::default()
        };

        let mut t: f32 = -0.5;
        let mut now = Instant::now();

        let mut framer = 0;
        let mut avg_fps = 0.0;
        let mut fps_counter = 0;

        while fps_counter < 10 {

            framer += 1;
            if now.elapsed().as_secs() >= 1 {
                avg_fps += 1.0/framer as f64;
                fps_counter += 1;
                framer = 0;
                now = Instant::now();
            }

            let (model, view_vectors, light) =  f(t);
            t += 0.02;

            let view = view_matrix(&view_vectors[0], &view_vectors[1], &view_vectors[2]);

            let mut target = display.draw();

            let perspective = calc_perspective(&target);

            target.clear_color_and_depth((0.0, 0.0, 0.0, 1.0), 1.0);
            target.draw(&self.shape , glium::index::NoIndices(glium::index::PrimitiveType::TrianglesList), &self.program,
            &uniform! { model: model, view: view, perspective: perspective, u_light: light }, &params).unwrap();
            target.finish().unwrap();

            for ev in display.poll_events() {
                match ev {
                    Event::Closed => {
                        return;
                    },
                    _ => ()

                }
            }

        }

        let seconds: f64 = now.elapsed().subsec_nanos() as f64/1_000_000_000.0;
        println!("{}", seconds);
        let result = avg_fps/fps_counter as f64;
        println!("Frame rate:{} ms, FPS:{}", result*1000.0, 1.0/result);
    }

}

use vertex;
use shaders;

use std::path::Path;

use mvp::{view_matrix, calc_perspective, create_textured_shape};

use std::time::{Instant};

use glium;
use glium::Surface;
use glium::glutin::{Event};

use image;

pub struct TexturedTest {
    shape: glium::VertexBuffer<vertex::TexturedVertex>,
    diffuse_texture: glium::texture::SrgbTexture2d,
    normal_map: glium::texture::Texture2d,
    program: glium::Program,
}

impl TexturedTest {
    pub fn new(display: &glium::backend::glutin_backend::GlutinFacade, shape_path: &str, texture_path: &str,
               normal_map_path: &str) -> TexturedTest {

        let image = image::open(&Path::new(texture_path)).unwrap().to_rgba();

        let image_dimensions = image.dimensions();
        let image = glium::texture::RawImage2d::from_raw_rgba_reversed(image.into_raw(), image_dimensions);

        let diffuse_texture = glium::texture::SrgbTexture2d::new(display, image).unwrap();

        let image = image::open(&Path::new(normal_map_path)).unwrap().to_rgba();
        let image_dimensions = image.dimensions();
        let image = glium::texture::RawImage2d::from_raw_rgba_reversed(image.into_raw(), image_dimensions);
        let normal_map = glium::texture::Texture2d::new(display, image).unwrap();

        let shape = create_textured_shape(shape_path);
        let shape = glium::VertexBuffer::new(display, &shape).unwrap();

        let shader = shaders::texture_shader::get_shader();
        let program = glium::Program::from_source(display, shader.vertex_shader_src, shader.fragment_shader_src, None).unwrap();

        TexturedTest {shape: shape, diffuse_texture: diffuse_texture, normal_map: normal_map, program: program}
    }

    pub fn run(self, display: &glium::backend::glutin_backend::GlutinFacade,
                            f: &Fn(f32) -> ([[f32; 4]; 4], [[f32; 3]; 3], [f32; 3])) {

        let params = glium::DrawParameters {
            depth: glium::Depth {
                test: glium::draw_parameters::DepthTest::IfLess,
                write: true,
                .. Default::default()
            },
            .. Default::default()
        };

        let mut t: f32 = -0.5;
        let mut now = Instant::now();

        let mut framer = 0;
        let mut avg_fps = 0.0;
        let mut fps_counter = 0;

        while fps_counter < 10 {

            framer += 1;
            if now.elapsed().as_secs() >= 1 {
                avg_fps += 1.0/framer as f64;
                fps_counter += 1;
                framer = 0;
                now = Instant::now();
            }

            let (model, view_vectors, light) =  f(t);
            t += 0.02;

            let view = view_matrix(&view_vectors[0], &view_vectors[1], &view_vectors[2]);

            let mut target = display.draw();

            let perspective = calc_perspective(&target);

            target.clear_color_and_depth((0.0, 0.0, 0.0, 1.0), 1.0);
            target.draw(&self.shape , glium::index::NoIndices(glium::index::PrimitiveType::TrianglesList), &self.program,
            &uniform! { model: model, view: view, perspective: perspective, u_light: light, diffuse_tex: &self.diffuse_texture, normal_tex: &self.normal_map }, &params).unwrap();

            target.finish().unwrap();


            for ev in display.poll_events() {
                match ev {
                    Event::Closed => {
                        return;
                    },
                    _ => ()

                }
            }

        }


        let seconds: f64 = now.elapsed().subsec_nanos() as f64/1_000_000_000.0;
        println!("{}", seconds);
        let result = avg_fps/fps_counter as f64;
        println!("Frame rate:{} ms, FPS:{}", result*1000.0, 1.0/result);
    }
}

#[derive(Copy, Clone, Debug)] 
pub struct Vertex {
    pub position: (f32, f32, f32),
    pub normal: (f32, f32, f32),
}

implement_vertex!(Vertex, position, normal);

#[derive(Copy, Clone, Debug)] 
pub struct TexturedVertex {
    pub position: (f32, f32, f32),
    pub normal: (f32, f32, f32),
    pub tex_coords: (f32, f32),
}

implement_vertex!(TexturedVertex, position, normal, tex_coords);

#[macro_use]
extern crate glium;
extern crate image;
extern crate xml;
extern crate rand;

mod vertex;
mod shaders;
mod mvp;
mod textured_test;
mod test;
mod sprite_test;
mod shading;

use glium::DisplayBuild;
use shading::Shading;
use std::env;
use std::process;

fn main() {
    let display;
    let mut is_fullscreen = false;
    let mut window_x: u32 = 800;
    let mut window_y: u32 = 600;
    let args: Vec<String> = env::args().collect();

    if args.len() > 1 {
        for arg in args.iter().skip(1) {
            let argument: Vec<&str> = arg.split("=").collect();
            if argument[0] == "--size" {
                let sizes: Vec<&str> = argument[1].split("x").collect();
                window_x = sizes[0].parse().unwrap();
                window_y = sizes[1].parse().unwrap();
            }
            if argument[0] == "--fullscreen" {
                is_fullscreen = true;
            }
            if argument[0] == "--help" {
                println!("Uruchamianie z użyciem cargo");
                println!("cargo run [--] [--size=800x600] [--fullscreen]");
                process::exit(0);
            }
        }
    } 

    if is_fullscreen {
        display = glium::glutin::WindowBuilder::new().with_dimensions(window_x, window_y)
            .with_fullscreen(glium::glutin::get_primary_monitor()).with_depth_buffer(24).build_glium().unwrap();
    } else {
        display = glium::glutin::WindowBuilder::new().with_dimensions(window_x, window_y)
            .with_depth_buffer(24).build_glium().unwrap();
    }
    

    let box_model = "./assets/models/textured_box.dae";
    let mountain_model = "./assets/models/gory.dae";
    let mountain_tex_model = "./assets/models/tex_mount.dae";
    let spade_model = "./assets/models/lopata2.dae";
    let teapot_model = "./assets/models/teapot.dae";

    let rust_texture = "./assets/textures/rust_tex.jpg";
    let rust_normal_mapping = "./assets/textures/rust_norm.jpg";

    let mount_texture = "./assets/textures/mount_tex.jpg";
    let mount_normal_mapping = "./assets/textures/mount_norm.jpg";
    

    println!("Loading Test 1");
    let test1 = test::Test::with_shading(&display, mountain_model, Shading::BlingPhong);
    println!("Loading Test 2");
    let test2 = textured_test::TexturedTest::new(&display, mountain_tex_model, mount_texture, mount_normal_mapping);
    println!("Loading Test 3");
    let test3 = test::Test::with_shading(&display, teapot_model, Shading::Cel);
    println!("Loading Test 4");
    let test4 = textured_test::TexturedTest::new(&display, box_model,  rust_texture,  rust_normal_mapping);
    println!("Loading Test 5");
    let test5 = test::Test::with_shading(&display, spade_model, Shading::Gouraud);
    println!("Loading Test 6");
    let mut test6 = sprite_test::SpriteTest::new(&display);

    use std::f32::consts::PI;

    test1.run(&display, &|t| {
        let x = PI/2.0;

        let model = [
            [ t.cos() , 0.0 , t.sin() , 0.0],
            [ t.sin()*x.sin() , x.cos() , -t.cos()*x.sin(), 0.0 ],
            [ -x.cos()*t.sin(), x.sin() , t.cos()*x.cos(), 0.0 ],
            [ 0.0, 0.0, 0.0, 1.0f32 ],
        ];

        let light = [-1.0, 0.4, 0.9f32];

        let view_vectors = [
            [1.0, 0.0, 0.0],
            [-0.1, -0.15, 0.0],            
            [0.0, 1.0, 0.0],
        ];

        (model, view_vectors, light)
    });


    test2.run(&display, &|t| {
        let x = PI/2.0;

        let model = [
            [ t.cos() , 0.0 , t.sin() , 0.0],
            [ t.sin()*x.sin() , x.cos() , -t.cos()*x.sin(), 0.0 ],
            [ -x.cos()*t.sin(), x.sin() , t.cos()*x.cos(), 0.0 ],
            [ 0.0, 0.0, 0.0, 1.0f32 ],
        ];

        let light = [-1.0, 0.4, 0.9f32];

        let view_vectors = [
            [1.0, 1.5, 0.0],
            [-0.1, -0.15, 0.0],            
            [0.0, 2.0, 0.0],
        ];

        (model, view_vectors, light)
    });


    test3.run(&display, &|t| {
        let x = PI/2.0;

        let model = [
            [ t.cos() , 0.0 , t.sin() , 0.0],
            [ t.sin()*x.sin() , x.cos() , -t.cos()*x.sin(), 0.0 ],
            [ -x.cos()*t.sin(), x.sin() , t.cos()*x.cos(), 0.0 ],
            [ 0.0, 0.0, 0.0, 1.0f32 ],
        ];

        let light = [4.0, 1.4, -0.5f32];

        let view_vectors = [
            [11.0, 6.0, 0.0],
            [-0.1, -0.05, 0.0],            
            [0.0, 1.0, 0.0],
        ];

        (model, view_vectors, light)
    });

    test4.run(&display, &|t| {

        let model = [
            [t.cos(), -t.sin(), 0.0 , 0.0],
            [t.sin(), t.cos(), 0.0, 0.0],
            [0.0, 0.0, 1.0 , 0.0],
            [0.0, 0.0, 0.0, 1.0f32]
        ];

            let light = [-1.0, 0.4, 0.9f32];

            let view_vectors = [
                [5.0, 0.0, 0.0],
                [-0.1, 0.0, 0.0],            
                [0.0, 1.0, 0.0],
            ];

                (model, view_vectors, light)
    });


    test5.run(&display, &|t| {
        let x = PI/2.0;

        let model = [
            [ t.cos() , 0.0 , t.sin() , 0.0],
            [ t.sin()*x.sin() , x.cos() , -t.cos()*x.sin(), 0.0 ],
            [ -x.cos()*t.sin(), x.sin() , t.cos()*x.cos(), 0.0 ],
            [ 0.0, 0.0, 0.0, 1.0f32 ],
        ];

        let light = [4.0, 1.4, -0.5f32];

        let view_vectors = [
            [11.0, 1.0, 0.0],
            [-0.1, 0.0, 0.0],            
            [0.0, 1.0, 0.0],
        ];

        (model, view_vectors, light)
    });

    test6.run(&display);

}

